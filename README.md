# Sport Events

A Retofit-RxJava Android application that consumes APIs at:
[https://www.thesportsdb.com/api.php](https://www.thesportsdb.com/api.php)

![demo](demo.gif)

#### Features
- Displays `Event`s by `League`s in a `Sport`
- Auto Refresh: Observable interval that refreshes events by current selected League
- Supports multiple Sport types: `Soccer`, `Ice Hockey`, `Basketball`, etc.
- [Picasso](https://square.github.io/picasso/) Image Loading/caching for league/team logos
- Login/Sign Up:Å
  - Support for login/sign up
  - Remembers user's favourite sports and selects it as default on loading
  - @Todo Keep favourite leagues and teams and make it rank higher

#### Structure
```
data
    - service: represents an RetroFit API endpoint/service
    
    - actions: action on a service/db that subscribes to Observable/Flowable
     and calls View methods when its subscriber receives data
     
    - db: Room Db and Data Access Objects
    
    - model: Data entities
    
view
    - activities: MainActivity, LoginActivity
    - fragments: DialogFragments
    - adapters: RecyclerView.Adapters
    
viewmodel
     - LoginViewModel
     - SportsEventsViewModel
     
```

#### Build and Run

Opening the project in Android Studio and Syncing Gradle dependencies should suffice to prepare 
the app for running on an Emulator/Connected Device.

