package com.soroush.test.sportsevents

import android.app.Application
import com.soroush.test.sportsevents.data.Prefs
import com.soroush.test.sportsevents.data.db.Db
import com.soroush.test.sportsevents.data.service.Services

class SportsEventsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        instantiateSingletons()
    }

    private fun instantiateSingletons() {
        Db.instantiate(applicationContext)
        Services.instantiate(applicationContext)
        Prefs.instantiate(applicationContext)
    }
}