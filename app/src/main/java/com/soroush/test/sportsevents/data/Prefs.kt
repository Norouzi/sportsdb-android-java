package com.soroush.test.sportsevents.data

import android.content.Context
import android.content.SharedPreferences
import com.soroush.test.sportsevents.helper.Constants

class Prefs private constructor(context: Context) {
    var isLoggedIn: Boolean
        get() = prefs.getBoolean(PrefKeys.IsLoggedIn, false)
        set(value) {
            prefs.edit().putBoolean(PrefKeys.IsLoggedIn, value).apply()
        }

    var currentUserEmail: String?
        get() = prefs.getString(PrefKeys.UserEmail, null)
        set(email) {
            prefs.edit().putString(PrefKeys.UserEmail, email).apply()
        }

    private object PrefKeys {
        const val IsLoggedIn = "prefKey_IsLoggedIn"
        const val UserEmail = "prefKey_UserEmail"
    }

    private val prefs: SharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)

    companion object {
        @JvmStatic
        fun instantiate(context: Context) {
            instance = Prefs(context)
        }

        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): Prefs? {
            checkNotNull(instance) { Constants.NOT_INSTANTIATED }
            return instance
        }

        private var instance: Prefs? = null
    }

}