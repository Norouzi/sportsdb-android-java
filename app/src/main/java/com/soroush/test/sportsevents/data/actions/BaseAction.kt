package com.soroush.test.sportsevents.data.actions

import android.util.Log

open class BaseAction {
    fun logError(e: Throwable) {
        Log.w(TAG, e.message)
    }

    companion object {
        private val TAG = LeagueActions::class.java.canonicalName
    }
}