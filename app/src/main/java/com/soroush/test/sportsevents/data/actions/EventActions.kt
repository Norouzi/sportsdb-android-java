package com.soroush.test.sportsevents.data.actions

import com.soroush.test.sportsevents.data.model.Event
import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.data.service.EventsService.Companion.getInstance
import com.soroush.test.sportsevents.helper.Constants.UPDATE_INTERVAL
import com.soroush.test.sportsevents.viewmodel.viewinterface.EventView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class EventActions(private val eventView: EventView) : BaseAction() {
    fun getEventsForLeague(leagueId: Int): Disposable {
        return getInstance().getPastEventsByLeagueId(leagueId)
            .subscribe(
                { events: ArrayList<Event>? ->
                    events?.let { eventView.setEvents(it) }
                }, ::logError
            )
    }


    fun updateEventsPeriodically(selectedLeague: League?): Disposable {
        return Observable.interval(0, UPDATE_INTERVAL, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { selectedLeague?.let { getEventsForLeague(it.idLeague) } },
                ::logError
            )
    }
}
