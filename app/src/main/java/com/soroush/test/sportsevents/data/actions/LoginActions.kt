package com.soroush.test.sportsevents.data.actions

import com.soroush.test.sportsevents.data.db.Db.getInstance
import com.soroush.test.sportsevents.data.model.User
import com.soroush.test.sportsevents.helper.Constants
import com.soroush.test.sportsevents.viewmodel.viewinterface.LoginView
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginActions(private val loginView: LoginView) {
    enum class AttemptStatus {
        STARTED, FAILED, SUCCEEDED
    }

    enum class Error {
        USER_ALREADY_EXISTS, WRONG_USERNAME_OR_PASSWORD, INTERNAL_ERROR
    }

    fun attemptLogin(userName: String, password: String): Disposable {
        loginView.setAttemptStart()
        return getInstance()!!.userDao()!!.getUser(userName, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { userList: List<User?>? ->
                    if (userList != null && !userList.isEmpty()) {
                        loginView.setAttemptSucceeded(userName)
                    } else if (userList == null) loginView.setAttemptFailed(Error.INTERNAL_ERROR) else loginView.setAttemptFailed(
                        Error.WRONG_USERNAME_OR_PASSWORD
                    )
                }
            ) { err: Throwable? -> loginView.setAttemptFailed(Error.INTERNAL_ERROR) }
    }

    fun attemptSignUp(userName: String, password: String): Disposable {
        require(!userName.isEmpty()) { String.format(Constants.CANNOT_BE_EMPTY, "userName") }
        require(!password.isEmpty()) { String.format(Constants.CANNOT_BE_EMPTY, "password") }
        loginView.setAttemptStart()
        return checkUserExists(userName)
            .subscribe(
                { userList: List<User?>? ->
                    if (userList != null && userList.isEmpty()) {
                        proceedToSignUp(userName, password)
                    } else if (userList == null) {
                        loginView.setAttemptFailed(Error.INTERNAL_ERROR)
                    } else {
                        loginView.setAttemptFailed(Error.USER_ALREADY_EXISTS)
                    }
                }
            ) { err: Throwable? -> loginView.setAttemptFailed(Error.INTERNAL_ERROR) }
    }

    /**********
     * Private
     */
    private fun checkUserExists(userName: String): Flowable<List<User?>?> {
        return getInstance()!!.userDao()!!.getUser(userName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun proceedToSignUp(userName: String, password: String): Disposable {
        return getInstance()!!.userDao()!!
            .insert(User(userName, password))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { loginView.setAttemptSucceeded(userName) }
            ) { err: Throwable? -> loginView.setAttemptFailed(Error.INTERNAL_ERROR) }
    }
}