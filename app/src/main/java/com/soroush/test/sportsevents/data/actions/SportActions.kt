package com.soroush.test.sportsevents.data.actions

import com.soroush.test.sportsevents.data.model.Sport
import com.soroush.test.sportsevents.data.service.SportService.Companion.getInstance
import com.soroush.test.sportsevents.helper.Constants.SUPPORTED_SPORT_NAMES
import com.soroush.test.sportsevents.viewmodel.viewinterface.SportView
import io.reactivex.disposables.Disposable
import java.util.*

class SportActions(private val sportView: SportView) : BaseAction() {
    fun fetchSports(): Disposable {
        return getInstance().allSports
            .subscribe({ sports: ArrayList<Sport> ->
                sportView.setSports(sports.filter {
                    SUPPORTED_SPORT_NAMES.contains(it.strSport)
                })
            }) { e: Throwable? ->
                e?.let { logError(it) }
            }
    }
}