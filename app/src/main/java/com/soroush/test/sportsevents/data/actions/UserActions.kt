package com.soroush.test.sportsevents.data.actions

import com.soroush.test.sportsevents.data.Prefs
import com.soroush.test.sportsevents.data.db.Db
import com.soroush.test.sportsevents.data.model.User
import com.soroush.test.sportsevents.viewmodel.viewinterface.UserView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class UserActions(private val userView: UserView) : BaseAction() {
    fun fetchUser(): Disposable {
        val userEmail = Prefs.getInstance()!!.currentUserEmail
        return Db.getInstance()!!.userDao()!!.getUser(userEmail)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { userList: List<User?>? ->
                    if (!userList.isNullOrEmpty()) userView.setUser(userList.first())
                }, ::logError
            )
    }

    fun updateUserFavSport(user: User?, favSport: String?): Disposable? {
        if (user != null) {
            user.favSport = favSport
            return Db.getInstance()!!.userDao()!!.update(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, ::logError)
        }
        return null
    }
}