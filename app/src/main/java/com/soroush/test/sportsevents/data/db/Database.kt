package com.soroush.test.sportsevents.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.soroush.test.sportsevents.data.db.dao.UserDao
import com.soroush.test.sportsevents.data.model.User

@Database(entities = [User::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun userDao(): UserDao?
}