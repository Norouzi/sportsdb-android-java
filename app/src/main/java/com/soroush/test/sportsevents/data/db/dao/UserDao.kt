package com.soroush.test.sportsevents.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.soroush.test.sportsevents.data.model.User
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE email=:email AND password=:password LIMIT 1")
    fun getUser(email: String?, password: String?): Flowable<List<User?>?>

    @Query("SELECT * FROM user WHERE email=:email LIMIT 1")
    fun getUser(email: String?): Flowable<List<User?>?>

    @Insert
    fun insert(users: User?): Completable

    @Update
    fun update(user: User?): Completable
}