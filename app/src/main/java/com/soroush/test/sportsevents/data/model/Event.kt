package com.soroush.test.sportsevents.data.model

import com.google.gson.annotations.SerializedName

class Event (
    @JvmField @SerializedName("strHomeTeam") val strHomeTeam: String,
    @JvmField @SerializedName("strAwayTeam") val strAwayTeam: String,
    @JvmField @SerializedName("intHomeScore") val intHomeScore: Int?,
    @JvmField @SerializedName("intAwayScore") val intAwayScore: Int?,
    @JvmField @SerializedName("dateEvent") val dateEvent: String?,
    @JvmField @SerializedName("strTime") val strTime: String?,
    @JvmField @SerializedName("idHomeTeam") val idHomeTeam: Int,
    @JvmField @SerializedName("idAwayTeam") val idAwayTeam: Int,
    @JvmField @SerializedName("strVideo") val strVideo: String?,
)