package com.soroush.test.sportsevents.data.model

import com.google.gson.annotations.SerializedName

data class League (
    @JvmField @SerializedName("idLeague") val idLeague: Int,
    @JvmField @SerializedName("strSport") val strSport: String,
    @JvmField @SerializedName("strLeague") val strLeague: String,
    @JvmField @SerializedName("strLogo") val strLogo: String?
)