package com.soroush.test.sportsevents.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Sport(
        @JvmField @SerializedName("idSport") val idSport: Int = 0,
        @JvmField @SerializedName("strSport") val strSport: String? = null,
        @JvmField @SerializedName("strFormat") val strFormat: String? = null,
        @JvmField @SerializedName("strSportThumb") val strSportThumb: String? = null,
        @JvmField @SerializedName("strSportDescription") val strSportDescription: String? = null,
) : Parcelable