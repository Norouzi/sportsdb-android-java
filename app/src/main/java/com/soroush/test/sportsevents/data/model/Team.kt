package com.soroush.test.sportsevents.data.model

import com.google.gson.annotations.SerializedName

data class Team(
        @JvmField @SerializedName("idTeam") var idTeam: Int,
        @JvmField @SerializedName("strTeamBadge") var strTeamBadge: String
)