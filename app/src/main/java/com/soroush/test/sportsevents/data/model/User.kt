package com.soroush.test.sportsevents.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soroush.test.sportsevents.helper.Constants

@Entity(tableName = Constants.DB_TABLE_USER)
class User(
        @field:ColumnInfo(name = "email") var email: String,
        @field:ColumnInfo(name = "password") var password: String
) {
    @JvmField @PrimaryKey(autoGenerate = true) var id = 0

    @JvmField @ColumnInfo(name = "favSport") var favSport: String? = null
}