package com.soroush.test.sportsevents.data.service

import android.content.Context
import com.soroush.test.sportsevents.data.model.Event
import com.soroush.test.sportsevents.data.service.Retrofit.Companion.getClient
import com.soroush.test.sportsevents.helper.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

class EventsService private constructor(context: Context) {

    @Deprecated("Available to Patreon subscribers only")
    fun getNextEventsByLeagueId(leagueId: Int): Observable<ArrayList<Event>?> {
        return api.getNextEventsByLeague(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { eventsResponse: EventsResponse -> if (eventsResponse.events != null) eventsResponse.events else ArrayList() }
    }

    fun getPastEventsByLeagueId(leagueId: Int): Observable<ArrayList<Event>?> {
        return api.getPastEventsByLeague(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { eventsResponse: EventsResponse -> eventsResponse.events }
    }

    private val api: EventsApi

    companion object {
        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): EventsService {
            checkNotNull(instance) { Constants.NOT_INSTANTIATED }
            return instance!!
        }

        /*******************
         * Package Private
         */
        fun instantiate(context: Context) {
            instance = EventsService(context)
        }

        private var instance: EventsService? = null
    }

    init {
        api = getClient(context)!!.create(
            EventsApi::class.java
        )
    }
}

internal interface EventsApi {

    @GET("eventsnextleague.php")
    @Deprecated("Available to Patreon subscribers only")
    fun getNextEventsByLeague(@Query("id") leagueId: Int): Observable<EventsResponse>

    @GET("eventspastleague.php")
    fun getPastEventsByLeague(@Query("id") leagueId: Int): Observable<EventsResponse>
}

internal class EventsResponse {
    var events: ArrayList<Event>? = null
}