package com.soroush.test.sportsevents.data.service

import android.content.Context
import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.data.service.Retrofit.Companion.getClient
import com.soroush.test.sportsevents.helper.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

class LeagueService private constructor(context: Context) {
    val allLeagues: Observable<ArrayList<League>?>
        get() = api.getAllLeagues()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { leaguesResponse: LeaguesResponse -> leaguesResponse.leagues }

    fun getLeagueDetail(leagueId: Int): Observable<League?> {
        return api.getLeagueDetail(leagueId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { leaguesResponse: LeaguesResponse -> leaguesResponse.leagues!![0] }
    }

    private val api: LeagueApi = getClient(context)!!.create(
        LeagueApi::class.java
    )

    companion object {
        /********************
         * Public
         */
        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): LeagueService {
            checkNotNull(instance) { Constants.NOT_INSTANTIATED }
            return instance!!
        }

        /********************
         * Package Private
         */
        fun instantiate(context: Context) {
            if (instance == null) {
                instance = LeagueService(context)
            }
        }

        /***********
         * Private
         */
        private var instance: LeagueService? = null
    }

}

internal class LeaguesResponse {
    var leagues: ArrayList<League>? = null
}

internal interface LeagueApi {
    @GET("all_leagues.php")
    fun getAllLeagues(): Observable<LeaguesResponse>

    @GET("lookupleague.php")
    fun getLeagueDetail(@Query("id") leagueId: Int): Observable<LeaguesResponse>
}