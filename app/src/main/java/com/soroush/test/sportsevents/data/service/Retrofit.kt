package com.soroush.test.sportsevents.data.service

import android.content.Context
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

internal class Retrofit private constructor(context: Context) {
    private val retrofitClient: retrofit2.Retrofit

    companion object {
        /*********
         * Public
         */
        @JvmStatic
        fun getClient(context: Context): retrofit2.Retrofit {
            return getInstance(context).retrofitClient
        }

        /**********
         * Private
         */
        private const val CACHE_SIZE = 10 * 1024 * 1024 // 10 MB
        private const val BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/"
        private var instance: Retrofit? = null
        private fun getInstance(context: Context): Retrofit {
            if (instance == null) instance = Retrofit(context)
            return instance!!
        }
    }

    init {
        val cache = Cache(context.cacheDir, CACHE_SIZE.toLong())
        val okHttpClient = OkHttpClient.Builder()
            .cache(cache)
            .build()
        val builder = retrofit2.Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        retrofitClient = builder.build()
    }
}