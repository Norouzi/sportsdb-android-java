package com.soroush.test.sportsevents.data.service

import android.content.Context
import com.soroush.test.sportsevents.data.model.Sport
import com.soroush.test.sportsevents.helper.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import java.util.*

class SportService private constructor(context: Context) {
    val allSports: Observable<ArrayList<Sport>>
        get() = api.allSports()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { sportsResponse: SportsResponse -> sportsResponse.sports }

    private val api: SportsApi = Retrofit.getClient(context).create(
        SportsApi::class.java
    )

    companion object {
        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): SportService {
            checkNotNull(instance) { Constants.NOT_INSTANTIATED }
            return instance!!
        }

        fun instantiate(context: Context) {
            if (instance == null) {
                instance = SportService(context)
            }
        }

        /**********
         * Private
         */
        private var instance: SportService? = null
    }

}

data class SportsResponse(
    var sports: ArrayList<Sport>
)

internal interface SportsApi {
    @GET("all_sports.php")
    fun allSports(): Observable<SportsResponse>
}