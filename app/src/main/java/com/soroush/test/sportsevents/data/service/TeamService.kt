package com.soroush.test.sportsevents.data.service

import android.content.Context
import com.soroush.test.sportsevents.data.model.Team
import com.soroush.test.sportsevents.helper.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.http.GET
import retrofit2.http.Query

class TeamService private constructor(context: Context) {
    fun getTeamsByLeagueName(leagueName: String?): Observable<List<Team>?> {
        return api.getTeamsByLeagueName(leagueName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { item: TeamResponse -> item.teams }
    }

    private val api: TeamApi = Retrofit.getClient(context).create(
        TeamApi::class.java
    )

    companion object {
        @JvmStatic
        @Throws(IllegalStateException::class)
        fun getInstance(): TeamService {
            checkNotNull(instance) { Constants.NOT_INSTANTIATED }
            return instance!!
        }

        fun instantiate(context: Context) {
            instance = TeamService(context)
        }

        private var instance: TeamService? = null
    }
}

internal interface TeamApi {
    @GET("search_all_teams.php")
    fun getTeamsByLeagueName(@Query("l") leagueName: String?): Observable<TeamResponse>
}

internal class TeamResponse {
    var teams: List<Team>? = null
}