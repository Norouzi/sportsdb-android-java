package com.soroush.test.sportsevents.helper

object Constants {
    /*
     * General
     */
    const val PREFS_NAME = "sportevets_prefs"
    const val DB_NAME = "sportevets_db_production"
    const val UPDATE_INTERVAL = 10L
    @JvmField val SUPPORTED_SPORT_NAMES = listOf(
        "Soccer", "Ice Hockey", "Basketball", "Rugby", "Australian Football",
        "Esports", "Darts", "Snooker", "Handball", "ESports", "Field Hockey",
        "Volleyball", "Netball", "Cricket", "Tennis", "American Football", "Baseball"
    )

    /*
     * Error Messages
     */
    const val NOT_INSTANTIATED =
        "Service has not been instantiated... call instantiate(Context context) first"

    const val CANNOT_BE_EMPTY = "'%s' cannot be empty!"

    /*
     * Request Codes
     */
    const val ACTIVITY_REQUEST_LOGIN = 0
    const val RESULT_SUCCESS = 1

    /*
     * DB Table Names
     */
    const val DB_TABLE_USER = "user"
}