package com.soroush.test.sportsevents.helper

import android.util.Log
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object Formatter {
    fun formatDateAndTime(_date: String, _time: String): String {
        var _time = _time
        _time = _time.replace("+00:00", "")
        val inFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val dateString = String.format("%sT%sZ", _date, _time)
        val format = SimpleDateFormat(inFormat, Locale.UK)
        try {
            val date = format.parse(dateString)
            return PrettyTime().format(date)
        } catch (e: ParseException) {
            Log.w(TAG, String.format("failed to parse from '%s' and '%s'", _date, _time))
        }
        return dateString
    }

    fun validateEmail(emailCandidate: String?): Boolean {
        val regex = "^(.+)@(.+)$"
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(emailCandidate)
        return matcher.matches()
    }

    private val TAG = Formatter::class.java.canonicalName
}