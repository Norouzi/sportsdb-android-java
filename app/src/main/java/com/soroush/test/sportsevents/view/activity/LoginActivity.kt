package com.soroush.test.sportsevents.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.actions.LoginActions
import com.soroush.test.sportsevents.data.actions.LoginActions.AttemptStatus
import com.soroush.test.sportsevents.databinding.ActivityLoginBinding
import com.soroush.test.sportsevents.helper.Constants
import com.soroush.test.sportsevents.helper.Formatter
import com.soroush.test.sportsevents.viewmodel.LoginViewModel

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        setupViews()
        setupCallbacks()
        setupViewModelObservers()
    }

    private fun setupViews() = with(binding) {
        editTextUserName.onFocusChangeListener =
            View.OnFocusChangeListener { _: View?, hasFocus: Boolean ->
                if (!hasFocus) {
                    val isGoodEmail = Formatter.validateEmail(editTextUserName.text.toString())
                    if (!isGoodEmail) editTextUserName.error =
                        resources.getString(R.string.invalid_email)
                }
            }
    }

    private fun setupCallbacks() = with(binding) {
        buttonLogin.setOnClickListener { viewModel.attemptLogin(userName, password) }
        buttonSignup.setOnClickListener { viewModel.attemptSignUp(userName, password) }
    }

    private fun setupViewModelObservers() = with(binding) {
        viewModel.getAttemptStatus().observe(this@LoginActivity, { status: AttemptStatus? ->
            when (status) {
                AttemptStatus.STARTED -> {
                    buttonSignup.visibility = View.GONE
                    buttonLogin.visibility = View.GONE
                    progress.visibility = View.VISIBLE
                }
                AttemptStatus.SUCCEEDED -> {
                    progress.visibility = View.GONE
                    setResult(Constants.RESULT_SUCCESS)
                    finish()
                }
                AttemptStatus.FAILED -> {
                    progress.visibility = View.GONE
                    buttonLogin.visibility = View.VISIBLE
                    buttonSignup.visibility = View.VISIBLE
                }
            }
        })
        viewModel.getError().observe(this@LoginActivity, { error: LoginActions.Error? ->
            if (error == null) return@observe
            when (error) {
                LoginActions.Error.USER_ALREADY_EXISTS -> textViewError.text = String.format(
                    resources.getString(R.string.user_already_exists),
                    userName
                )
                LoginActions.Error.WRONG_USERNAME_OR_PASSWORD -> textViewError.text =
                    resources.getString(R.string.wrong_credentials)
                LoginActions.Error.INTERNAL_ERROR -> textViewError.text =
                    resources.getString(R.string.internal_error_message)
            }
        })
    }

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    private val userName: String
        get() = binding.editTextUserName.text.toString().trim { it <= ' ' }

    private val password: String
        get() = binding.editTextPassword.text.toString().trim { it <= ' ' }

    companion object {
        @JvmStatic
        fun startForResult(activity: Activity, requestCode: Int) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivityForResult(intent, requestCode)
        }
    }
}