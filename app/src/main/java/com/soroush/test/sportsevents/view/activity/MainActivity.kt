package com.soroush.test.sportsevents.view.activity

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.model.Event
import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.data.model.Sport
import com.soroush.test.sportsevents.databinding.ActivityMainBinding
import com.soroush.test.sportsevents.helper.Constants
import com.soroush.test.sportsevents.view.activity.LoginActivity.Companion.startForResult
import com.soroush.test.sportsevents.view.adapters.EventFeedbackListener
import com.soroush.test.sportsevents.view.adapters.EventRecyclerViewAdapter
import com.soroush.test.sportsevents.view.adapters.LeagueRecyclerViewAdapter
import com.soroush.test.sportsevents.view.adapters.OnLeagueSelectedListener
import com.soroush.test.sportsevents.view.fragment.SportSelectionDialogFragment.Companion.show
import com.soroush.test.sportsevents.view.fragment.SportSelectionListener
import com.soroush.test.sportsevents.viewmodel.SportsEventsViewModel
import com.squareup.picasso.Picasso
import java.util.*

class MainActivity : AppCompatActivity(), OnLeagueSelectedListener, SportSelectionListener,
    EventFeedbackListener {
    /*****************************
     * AppCompatActivity Methods
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProviders.of(this).get(SportsEventsViewModel::class.java)
        setupViews()
        subscribeObservers()
        setupOnClickListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.ACTIVITY_REQUEST_LOGIN && resultCode == Constants.RESULT_SUCCESS) {
            viewModel.setUserLoggedIn(true)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /****************************
     * OnLeagueSelectedListener
     */
    override fun onLeagueSelected(league: League) {
        viewModel.setSelectedLeague(league)
    }

    /*************************
     * SportSelectionListener
     */
    override fun onSportSelected(sport: Sport) {
        viewModel.setSelectedSport(sport)
    }

    /*************************
     * EventFeedbackListener
     */
    override fun onVideoLinkClicked(url: String?) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(url)
            )
        )
    }

    /**********
     * Private
     */
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: SportsEventsViewModel
    private lateinit var leagueRecyclerViewAdapter: LeagueRecyclerViewAdapter
    private lateinit var eventRecyclerViewAdapter: EventRecyclerViewAdapter

    private fun setupViews() {
        leagueRecyclerViewAdapter = LeagueRecyclerViewAdapter(this)
        eventRecyclerViewAdapter = EventRecyclerViewAdapter(this)
        binding.recyclerViewLeagues.adapter = leagueRecyclerViewAdapter
        binding.recyclerViewEvents.adapter = eventRecyclerViewAdapter
    }

    private fun subscribeObservers() {
        // user
        viewModel.getIsUserLoggedIn().observe(this, { isLoggedIn: Boolean ->
            with(binding.imageViewLogin) {
                setColorFilter(
                    ContextCompat.getColor(
                        this@MainActivity,
                        if (isLoggedIn) R.color.accentColor else R.color.primaryDark
                    )
                )
                alpha = if (isLoggedIn) 1f else 0.5f
            }
        })

        // sport
        val viewSplash = findViewById<View>(R.id.layout_splash)
        viewModel.getSelectedSport().observe(this, { sport: Sport? ->
            if (sport != null) {
                binding.textViewSportTitle.text = String.format("⋮ %s", sport.strSport)
                if (sport.strSportThumb != null && sport.strSportThumb.isNotEmpty())
                    Picasso.get()
                        .load(sport.strSportThumb)
                        .placeholder(R.drawable.ic_image)
                        .error(R.drawable.ic_image)
                        .fit()
                        .into(binding.imageViewSportLogo)
                viewSplash.visibility = View.GONE
            } else {
                viewSplash.visibility = View.VISIBLE
            }
        })

        // leagues
        viewModel.getLeagues()
            .observe(this, { leagues: List<League>? ->
                with(binding) {
                    if (leagues == null) {
                        progressLeagues.visibility = View.VISIBLE
                        recyclerViewLeagues.visibility = View.INVISIBLE
                        separator.visibility = View.INVISIBLE
                    } else {
                        progressLeagues.visibility = View.GONE
                        recyclerViewLeagues.visibility = View.VISIBLE
                        leagueRecyclerViewAdapter.setLeagues(
                            leagues,
                            viewModel.getSelectedLeague().value
                        )
                        separator.visibility = View.VISIBLE
                    }
                }
            })

        viewModel.getSelectedLeague().observe(this, { league: League? ->
            viewModel.getLeagues().value?.let {
                leagueRecyclerViewAdapter.setLeagues(
                    it, league
                )
            }
        })

        // events
        viewModel.getEvents().observe(this, { events: List<Event>? ->
            with(binding) {
                when {
                    events == null -> {
                        recyclerViewEvents.visibility = View.INVISIBLE
                        progressEvents.visibility = View.VISIBLE
                        textViewNoUpcomingEvents.visibility = View.INVISIBLE
                    }
                    events.isNotEmpty() -> {
                        progressEvents.visibility = View.GONE
                        recyclerViewEvents.visibility = View.VISIBLE
                        eventRecyclerViewAdapter.setData(events, viewModel.getTeamLogos().value)
                        textViewNoUpcomingEvents.visibility = View.INVISIBLE
                    }
                    else -> {
                        progressEvents.visibility = View.GONE
                        recyclerViewEvents.visibility = View.INVISIBLE
                        textViewNoUpcomingEvents.visibility = View.VISIBLE
                        var leagueName = resources.getString(R.string.league)
                        viewModel.getSelectedLeague().value?.let { leagueName = it.strLeague }
                        textViewNoUpcomingEvents.text = String.format(
                            resources.getString(R.string.no_upcoming_events),
                            leagueName
                        )
                    }
                }
            }
        })

        viewModel.getTeamLogos()
            .observe(this, { logos: Map<Int, String>? ->
                eventRecyclerViewAdapter.setData(
                    viewModel.getEvents().value, logos
                )
            })
    }

    private fun setupOnClickListeners() = with(binding) {
        textViewSportTitle.setOnClickListener { view: View? ->
            show(
                supportFragmentManager,
                viewModel.getSports().value,
                viewModel.getSelectedSport().value
            )
        }
        imageViewLogin.setOnClickListener { v: View? ->
            if (viewModel.getIsUserLoggedIn().value != null) {
                val isLoggedIn = viewModel.getIsUserLoggedIn().value ?: false
                if (isLoggedIn) showLogoutDialog() else startForResult(
                    this@MainActivity,
                    Constants.ACTIVITY_REQUEST_LOGIN
                )
            }
        }
    }

    private fun showLogoutDialog() {
        AlertDialog.Builder(this)
            .setMessage(resources.getString(R.string.logout_areyousure))
            .setPositiveButton(
                resources.getString(R.string.yes)
            ) { _: DialogInterface?, _: Int -> viewModel.logOut() }
            .setNegativeButton(
                resources.getString(R.string.no)
            ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
            .show()
    }
}