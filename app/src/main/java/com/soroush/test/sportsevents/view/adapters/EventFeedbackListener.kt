package com.soroush.test.sportsevents.view.adapters

interface EventFeedbackListener {
    fun onVideoLinkClicked(url: String?)
}