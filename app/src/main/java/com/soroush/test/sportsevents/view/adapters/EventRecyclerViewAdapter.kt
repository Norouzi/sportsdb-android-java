package com.soroush.test.sportsevents.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.model.Event
import com.soroush.test.sportsevents.databinding.ViewholderEventBinding
import com.soroush.test.sportsevents.helper.Formatter.formatDateAndTime
import com.squareup.picasso.Picasso
import java.util.*

class EventRecyclerViewAdapter(private val listener: EventFeedbackListener) :
    RecyclerView.Adapter<EventViewHolder>() {

    fun setData(events: List<Event>?, teamLogos: Map<Int, String?>?) {
        if (events != null && teamLogos != null) {
            this.events = events
            this.teamLogos = teamLogos
            notifyDataSetChanged()
        }
    }

    /********************************
     * RecyclerView.Adapter Methods
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(ViewholderEventBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) = with(holder.binding) {
        val event = events[position]

        // Time
        textViewEventTime.text = formatDateAndTime(
            event.dateEvent ?: "",
            event.strTime ?: ""
        )

        // Video Link
        if (event.strVideo != null && event.strVideo.toLowerCase().contains("youtube")) {
            imageViewVideo.setOnClickListener { v: View? -> listener.onVideoLinkClicked(event.strVideo) }
            imageViewVideo.visibility = View.VISIBLE
        } else {
            imageViewVideo.visibility = View.GONE
        }

        // HOME
        val homeIsWinning =
            event.intHomeScore != null && event.intAwayScore != null && event.intHomeScore > event.intAwayScore
        val awayIsWinning =
            event.intHomeScore != null && event.intAwayScore != null && event.intHomeScore < event.intAwayScore
        var textColor = ContextCompat.getColor(
            root.context,
            if (homeIsWinning) R.color.accentColor else R.color.primaryDark
        )
        textViewHomeTitle.text = event.strHomeTeam
        textViewHomeTitle.setTextColor(textColor)
        textViewHomeScore.text =
            if (event.intHomeScore != null) event.intHomeScore.toString() else "-"
        textViewHomeScore.setTextColor(textColor)
        if (teamLogos != null && teamLogos!![event.idHomeTeam] != null && !teamLogos!![event.idHomeTeam]!!
                .isEmpty()
        ) {
            Picasso.get()
                .load(teamLogos!![event.idHomeTeam])
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .fit()
                .into(imageViewHomeLogo)
            imageViewHomeLogo.visibility = View.VISIBLE
        } else {
            imageViewHomeLogo.visibility = View.INVISIBLE
        }

        // AWAY
        textColor = ContextCompat.getColor(
            root.context,
            if (awayIsWinning) R.color.accentColor else R.color.primaryDark
        )
        textViewAwayTitle.text = event.strAwayTeam
        textViewAwayTitle.setTextColor(textColor)
        if (teamLogos != null && teamLogos!![event.idAwayTeam] != null && teamLogos!![event.idAwayTeam]!!.isNotEmpty()
        ) {
            Picasso.get()
                .load(teamLogos!![event.idAwayTeam])
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .fit()
                .into(imageViewAwayLogo)
            imageViewAwayLogo.visibility = View.VISIBLE
        } else {
            imageViewAwayLogo.visibility = View.INVISIBLE
        }
        textViewAwayScore.text =
            if (event.intAwayScore != null) event.intAwayScore.toString() else "-"
        textViewAwayScore.setTextColor(textColor)
    }

    override fun getItemCount(): Int {
        return events.size
    }

    /***********
     * Private
     */
    private var events: List<Event> = ArrayList()
    private var teamLogos: Map<Int, String?>? = HashMap()
}

class EventViewHolder(val binding: ViewholderEventBinding) : ViewHolder(binding.root)