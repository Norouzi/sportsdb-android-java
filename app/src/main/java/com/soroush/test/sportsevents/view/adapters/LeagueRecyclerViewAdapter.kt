package com.soroush.test.sportsevents.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.databinding.ViewholderLeagueBinding
import com.squareup.picasso.Picasso

class LeagueRecyclerViewAdapter(listener: OnLeagueSelectedListener) :
    RecyclerView.Adapter<LeagueViewHolder>() {
    fun setLeagues(leagues: List<League>, selectedLeague: League?) {
        this.leagues = leagues
        this.selectedLeague = selectedLeague
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LeagueViewHolder(ViewholderLeagueBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: LeagueViewHolder, position: Int) = with(holder.binding) {
        val league = leagues[position]
        selectedLeague?.let {
            root.setBackgroundColor(
                ContextCompat.getColor(
                    root.context,
                    if (league.idLeague == it.idLeague)
                        R.color.accentColor else R.color.backgroundColorDark
                )
            )
        }
        root.setOnClickListener { listener?.onLeagueSelected(league) }
        textViewLeagueTitle.text = league.strLeague

        if (league.strLogo?.isNotBlank() == true) {
            val imageView = holder.itemView.findViewById<ImageView>(R.id.imageView_league_logo)
            Picasso.get()
                .load(String.format("%s/preview", league.strLogo))
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .fit()
                .into(imageView)
        }
    }

    override fun getItemCount(): Int {
        return leagues.size
    }

    /***********
     * Private
     */
    private var leagues = listOf<League>()
    private var selectedLeague: League? = null
    private val listener: OnLeagueSelectedListener?

    init {
        this.listener = listener
    }
}

class LeagueViewHolder(val binding: ViewholderLeagueBinding) : ViewHolder(binding.root)