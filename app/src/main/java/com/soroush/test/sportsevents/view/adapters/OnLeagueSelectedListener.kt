package com.soroush.test.sportsevents.view.adapters

import com.soroush.test.sportsevents.data.model.League

interface OnLeagueSelectedListener {
    fun onLeagueSelected(league: League)
}