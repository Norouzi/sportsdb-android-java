package com.soroush.test.sportsevents.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.model.Sport
import com.soroush.test.sportsevents.databinding.ViewholderSportBinding
import com.soroush.test.sportsevents.view.fragment.SportSelectionListener
import com.squareup.picasso.Picasso
import java.util.*

class SportRecyclerViewAdapter(
    private val sports: List<Sport>,
    private var selectedSport: Sport?,
    private val listener: SportSelectionListener?
) : RecyclerView.Adapter<SportViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SportViewHolder(ViewholderSportBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: SportViewHolder, position: Int) = with(holder.binding) {
        val sport = sports[position]
        val isSelected = sport.idSport == selectedSport?.idSport

        Picasso.get()
            .load(sport.strSportThumb)
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_image)
            .fit()
            .into(imageViewSportLogo)
        imageViewSportLogo.setOnClickListener {
            selectedSport = sport
            notifyDataSetChanged()
            listener?.onSportSelected(sport)
        }
        textViewSportTitle.text = sport.strSport
        textViewSportTitle.setTextColor(
            ContextCompat.getColor(root.context,
                if (isSelected) R.color.accentColor else R.color.primary
            )
        )
        imageViewSelected.visibility = if (isSelected) View.VISIBLE else View.INVISIBLE
    }

    override fun getItemCount() = sports.size
}

class SportViewHolder(val binding: ViewholderSportBinding) : ViewHolder(binding.root)