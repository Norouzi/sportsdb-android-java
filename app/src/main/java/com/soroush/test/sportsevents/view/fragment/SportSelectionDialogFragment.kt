package com.soroush.test.sportsevents.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.soroush.test.sportsevents.R
import com.soroush.test.sportsevents.data.model.Sport
import com.soroush.test.sportsevents.view.adapters.SportRecyclerViewAdapter
import java.util.*

class SportSelectionDialogFragment : DialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sports = arguments?.getParcelableArray(ARG_SPORTS)?.toList() as List<Sport>?
        selectedSport = arguments?.getParcelable(ARG_SELECTED_SPORT) as Sport?
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_sport_select, container, false)
        val recyclerView: RecyclerView = v.findViewById(R.id.recyclerView_sports)
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        sports?.let { sports ->
            recyclerView.adapter = SportRecyclerViewAdapter(sports, selectedSport,
                object : SportSelectionListener {
                    override fun onSportSelected(sport: Sport) {
                        listener?.onSportSelected(sport)
                        dismiss()
                    }
                })
        }

        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SportSelectionListener) listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private var sports: List<Sport>? = null
    private var selectedSport: Sport? = null
    private var listener: SportSelectionListener? = null

    companion object {
        @JvmStatic
        fun show(fm: FragmentManager, sports: List<Sport>?, selectedSport: Sport?) {
            // instantiate
            val f = SportSelectionDialogFragment()

            // build args
            val args = Bundle()
            args.putParcelableArray(ARG_SPORTS, sports?.toTypedArray())
            args.putParcelable(ARG_SELECTED_SPORT, selectedSport)
            f.arguments = args

            // show fragment (+ replace existing previous instance)
            val ft = fm.beginTransaction()
            val prev = fm.findFragmentByTag(TAG)
            if (prev != null) ft.remove(prev)
            ft.addToBackStack(null)
            f.show(ft, TAG)
        }

        private const val ARG_SPORTS = "args_sports"
        private const val ARG_SELECTED_SPORT = "args_selected_sport"
        private val TAG = SportSelectionDialogFragment::class.java.canonicalName
    }
}