package com.soroush.test.sportsevents.view.fragment

import com.soroush.test.sportsevents.data.model.Sport

interface SportSelectionListener {
    fun onSportSelected(sport: Sport)
}