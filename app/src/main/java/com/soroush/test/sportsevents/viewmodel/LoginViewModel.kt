package com.soroush.test.sportsevents.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.soroush.test.sportsevents.data.Prefs.Companion.getInstance
import com.soroush.test.sportsevents.data.actions.LoginActions
import com.soroush.test.sportsevents.data.actions.LoginActions.AttemptStatus
import com.soroush.test.sportsevents.viewmodel.viewinterface.LoginView
import io.reactivex.disposables.Disposable

class LoginViewModel : ViewModel(), LoginView {
    override fun setAttemptStart() {
        attemptStatus.value = AttemptStatus.STARTED
        error.value = null
    }

    override fun setAttemptSucceeded(email: String) {
        getInstance()?.isLoggedIn = true
        getInstance()?.currentUserEmail = email
        attemptStatus.value = AttemptStatus.SUCCEEDED
    }

    override fun setAttemptFailed(error: LoginActions.Error) {
        attemptStatus.value = AttemptStatus.FAILED
        this.error.value = error
    }

    fun getAttemptStatus(): LiveData<AttemptStatus> {
        return attemptStatus
    }

    fun getError(): LiveData<LoginActions.Error?> {
        return error
    }

    fun attemptLogin(userName: String, password: String): Disposable {
        return loginActions.attemptLogin(userName, password)
    }

    fun attemptSignUp(userName: String, password: String): Disposable {
        return loginActions.attemptSignUp(userName, password)
    }

    /***********
     * Private
     */
    private val loginActions: LoginActions = LoginActions(this)
    private val attemptStatus = MutableLiveData<AttemptStatus>()
    private val error = MutableLiveData<LoginActions.Error?>()
}