package com.soroush.test.sportsevents.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.soroush.test.sportsevents.data.Prefs.Companion.getInstance
import com.soroush.test.sportsevents.data.actions.EventActions
import com.soroush.test.sportsevents.data.actions.LeagueActions
import com.soroush.test.sportsevents.data.actions.SportActions
import com.soroush.test.sportsevents.data.actions.UserActions
import com.soroush.test.sportsevents.data.model.*
import com.soroush.test.sportsevents.viewmodel.viewinterface.EventView
import com.soroush.test.sportsevents.viewmodel.viewinterface.LeagueView
import com.soroush.test.sportsevents.viewmodel.viewinterface.SportView
import com.soroush.test.sportsevents.viewmodel.viewinterface.UserView
import java.util.*

class SportsEventsViewModel : ViewModel(), UserView, SportView, LeagueView, EventView {
    /// User
    fun getIsUserLoggedIn(): LiveData<Boolean> {
        return isUserLoggedIn
    }

    fun setUserLoggedIn(isLoggedIn: Boolean) {
        isUserLoggedIn.value = isLoggedIn
        if (isLoggedIn) userActions.fetchUser()
    }

    fun logOut() {
        isUserLoggedIn.value = false
        getInstance()?.isLoggedIn = false
        getInstance()!!.currentUserEmail = null
        setUser(null)
    }

    override fun setUser(user: User?) {
        this.user.value = user
        if (user != null) setSelectedSportByName(user.favSport)
        else setSports(getSports().value!!)
    }

    /// SPORT
    fun getSports(): LiveData<List<Sport>> {
        return sports
    }

    fun getSelectedSport(): LiveData<Sport?> {
        return selectedSport
    }

    fun setSelectedSport(sport: Sport) {
        if (selectedSport.value == null || selectedSport.value!!.idSport != sport.idSport) {
            updateSelectedSport(sport)
            userActions.updateUserFavSport(user.value, sport.strSport)
        }
    }

    override fun setSports(sports: List<Sport>) {
        this.sports.value = sports
        val favSport = if (user.value != null) user.value!!.favSport else null
        if (favSport == null) setSelectedSport(sports[0]) else setSelectedSportByName(favSport)
    }

    /// LEAGUE
    fun getLeagues(): LiveData<List<League>?> {
        return leagues
    }

    fun getSelectedLeague(): LiveData<League?> {
        return selectedLeague
    }

    override fun setSelectedLeague(league: League) {
        if (selectedLeague.value == null || selectedLeague.value!!.idLeague != league.idLeague) {
            events.value = null
            selectedLeague.value = league
            leagueActions.getTeamsForLeague(league.strLeague)
            eventActions.getEventsForLeague(league.idLeague)
        }
    }

    fun getTeamLogos(): LiveData<Map<Int, String>> {
        return teamLogos
    }

    override fun setLeagues(leagues: List<League>) {
        this.leagues.value = leagues
        leagueActions.populateLeagueDetails(leagues)
    }

    override fun updateLeagueDetail(league: League) {
        leagues.value = getLeagues().value?.map {
            if (it.idLeague == league.idLeague) league else it
        } ?: listOf()
    }

    override fun setLeagueTeams(teams: List<Team>) {
        leagueTeams.value = teams
    }

    override fun setTeamLogos(logoMap: Map<Int, String>) {
        teamLogos.value = logoMap
    }

    /// EVENTS
    fun getEvents(): LiveData<List<Event>?> {
        return events
    }

    override fun setEvents(events: List<Event>) {
        this.events.value = events
    }

    /***********
     * Private
     */
    /// Sport
    private fun setSelectedSportByName(sportName: String?) {
        val sports: List<Sport>? = sports.value
        if (sports != null) {
            for (sport in sports) {
                if (sport.strSport == sportName) {
                    updateSelectedSport(sport)
                    return
                }
            }
        }
    }

    private fun updateSelectedSport(sport: Sport) {
        selectedSport.value = sport
        leagues.value = null
        events.value = null
        leagueActions.getLeaguesForSportName(sport.strSport!!)
    }

    /// Actions
    private val userActions: UserActions = UserActions(this)
    private val leagueActions: LeagueActions
    private val eventActions: EventActions

    /// Live Data
    private val isUserLoggedIn = MutableLiveData<Boolean>()
    private val user = MutableLiveData<User?>()
    private val sports = MutableLiveData<List<Sport>>()
    private val selectedSport = MutableLiveData<Sport?>()
    private val leagues = MutableLiveData<List<League>?>()
    private val selectedLeague = MutableLiveData<League?>()
    private val leagueTeams = MutableLiveData<List<Team>>()
    private val teamLogos = MutableLiveData<Map<Int, String>>()
    private val events = MutableLiveData<List<Event>?>()

    /// Constructor
    init {
        val sportActions = SportActions(this)
        eventActions = EventActions(this)
        leagueActions = LeagueActions(this)
        setUserLoggedIn(getInstance()?.isLoggedIn ?: false)
        sportActions.fetchSports()
        eventActions.updateEventsPeriodically(selectedLeague.value)
    }
}