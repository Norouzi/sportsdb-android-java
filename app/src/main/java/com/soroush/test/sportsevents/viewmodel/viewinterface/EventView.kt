package com.soroush.test.sportsevents.viewmodel.viewinterface

import com.soroush.test.sportsevents.data.model.Event

interface EventView {
    fun setEvents(events: List<Event>)
}