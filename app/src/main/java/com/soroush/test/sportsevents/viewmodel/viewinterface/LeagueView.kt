package com.soroush.test.sportsevents.viewmodel.viewinterface

import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.data.model.Team
import java.util.*

interface LeagueView {
    fun setLeagues(leagues: List<League>)
    fun setSelectedLeague(league: League)
    fun updateLeagueDetail(leage: League)
    fun setLeagueTeams(teams: List<Team>)
    fun setTeamLogos(logoMap: Map<Int, String>)
}