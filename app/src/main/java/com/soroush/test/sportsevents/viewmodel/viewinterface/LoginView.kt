package com.soroush.test.sportsevents.viewmodel.viewinterface

import com.soroush.test.sportsevents.data.actions.LoginActions

interface LoginView {
    fun setAttemptStart()
    fun setAttemptSucceeded(userName: String)
    fun setAttemptFailed(error: LoginActions.Error)
}