package com.soroush.test.sportsevents.viewmodel.viewinterface

import com.soroush.test.sportsevents.data.model.Sport
import java.util.*

interface SportView {
    fun setSports(sports: List<Sport>)
}