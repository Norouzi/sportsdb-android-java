package com.soroush.test.sportsevents.viewmodel.viewinterface

import com.soroush.test.sportsevents.data.model.User

interface UserView {
    fun setUser(user: User?)
}